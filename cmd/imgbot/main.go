package main

import (
	"gitea.z0ne.moe/cking/imgbot/internal/app"
)

func main() {
	a := app.New()
	a.Run()
}

/*
func main() {
	flag.Parse()
	if *version {
		fmt.Printf(
			"%v%v\n  -- %v %v/%v\n",
			config.ApplicationName, config.Version,
			runtime.Version(), runtime.GOOS, runtime.GOARCH,
		)

		return
	}
	run()
}

func run() {
	cfg := new(config.Config)
	err := cfg.Load(*configFile)
	if err != nil {
		panic(err)
	}

	container := services.Build(cfg)
	logger := container.Logger()
	compiled, _ := time.Parse(time.RFC3339, config.BuildDate)

	logger.Info(
		config.ApplicationName+" starting up",
		zap.Time("compiled", compiled),
		zap.String("version", config.LongVersion),
	)

	logger.Debug("creating asset directory", zap.String("dir", cfg.Assets))
	err = os.MkdirAll(cfg.Assets, os.ModePerm)
	if err != nil {
		logger.Fatal("failed to create asset directory", zap.Error(err), zap.String("dir", cfg.Assets))
	}

	logger.Debug("creating database connection")
	db := container.Storm()

	var pc *pixiv.AppPixivAPI
	if cfg.Pixiv.IsEnabled() {
		logger.Debug("connecting to pixiv")
		_, err := pixiv.Login(cfg.Pixiv.Username, cfg.Pixiv.Password)
		if err != nil {
			logger.Fatal("failed to connect to pixiv", zap.Error(err))
		}
		logger.Info("logged in to pixiv")
		pc = pixiv.NewApp()
	}

	logger.Debug("connecting to mastodon")
	m := container.Mastodon()
	acc, err := m.GetAccountCurrentUser(context.Background())
	if err != nil {
		logger.Fatal("failed to connect to mastodon", zap.Error(err))
	}
	logger.Info("logged in to mastodon", zap.String("acct", acc.Acct))

	logger.Debug("creating app")
	a.OnInit(func(app *grumble.App, flags grumble.FlagMap) error {
		return nil
	})
		a := app.New(logger, m, pc, db, cfg.Assets)
		if *one {
			a.Post()
			return
		} else if !*batch && isatty.IsTerminal(os.Stdout.Fd()) {
			a.Interactive()
		}

		a.Run()
}
*/
