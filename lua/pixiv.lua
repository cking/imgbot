local c = require("config")

local http = require("http")
local log = require("logger")
local url = require("url")
local json = require("json")
local fs = require("fs")
local fp = require("filepath")
local db = require("db")
local inspect = require("inspect")
local survey = require("survey")
local crypto = require("crypto")
local os = require("os")

local clientID     = "MOBrBDS8blbauoSck0ZfDbtuzpyT"
local clientSecret = "lsACyCD94FhDUtGTXi3QzcFE2uU1hqtDaKeqrdwj"
local clientHash = "28c1fdd170a5204386cb1313c7077b34f83e4aaf4aa829ce78c231e05b0bae2c"

local userAgent = "PixivAndroidApp/5.0.64 (Android 6.0)"

function login()
  local ts = os.date("%Y-%m-%dT%H:%M:%SZ", os.time())
  local hash = crypto.md5(ts .. clientHash)

  local res = http.post("https://oauth.secure.pixiv.net/auth/token", {
    headers = {
      ["User-Agent"] = userAgent,
      ["Content-Type"] = "application/x-www-form-urlencoded",
      ["X-Client-Time"] = ts,
      ["X-Client-Hash"] = hash
    },
    body = url.build_query_string({
      get_secure_url = 1,
      client_id = clientID,
      client_secret = clientSecret,
      grant_type = "password",
      username = c.pixiv.username,
      password = c.pixiv.password
    })
  })
  log.printDebug("[PIXIV] %v %v", res.status_code, "https://oauth.secure.pixiv.net/auth/token")
  if res.status_code ~= 200 then error("failed to fetch access token") end
  local j = json.decode(res.body)
  return j.response.access_token
end

function illust(id, accessToken)
  local res = http.get("https://app-api.pixiv.net/v1/illust/detail", {
    headers = {
      ["User-Agent"] = "PixivIOSApp/6.7.1 (iOS 10.3.1; iPhone8,1)",
      ["App-Version"] = "6.7.1",
      ["App-OS-VERSION"] = "10.3.1",
      ["App-OS"] = "ios",
      ["Authorization"] = "Bearer " .. accessToken,
      ["Accept-Language"] = "en"
    },
    query = url.build_query_string({
      illust_id = id
    })
  })
  log.printDebug("[PIXIV] %v %v", res.status_code, "https://app-api.pixiv.net/v1/illust/detail")
  if res.status_code ~= 200 then error("failed to fetch illust details") end
  return json.decode(res.body).illust
end

function main(args)
  if table.getn(args) ~= 1 then
    log.printWarning("please supply a single member illust link")
    return
  end

  local illustID = url.parse(args[1]).query
  illustID = illustID:sub(illustID:find("illust_id=") + 10)

  local accessToken = login()
  illust = illust(illustID, accessToken)

  local iuri = illust.meta_single_page.original_image_url
  if table.getn(illust.meta_pages) > 0 then
    local val, answered = survey.input("Which image to use?", "1")
    if not answered then
        log.printWarning("no image selected, aborting")
        return
    end
    iuri = illust.meta_pages[tonumber(val)].image_urls.original
  end


  local res = http.get(iuri, {
    headers = {
      Referer = args[1]
    }
  })
  log.printDebug("[PIXIV] %v %v", res.status_code, iuri)
  local file = db.image_file(fp.ext(iuri))
  fs.write(file, res.body)
  log.printDebug("saved new image file at %v", file)

  local image = {
    File = file,
    Source = args[1],
    IsNSFW = illust.restrict > 0,
    Tags = {},
    Author = {
      Name = illust.user.name .. " (" .. illust.user.account .. ")",
      Contact = "https://www.pixiv.net/member.php?id=" .. illust.user.id
    }
  }

  for i, tag in ipairs(illust.tags) do
    table.insert(image.Tags, tag.name)

    if not db.has_tag(tag.name) then
      local alias = db.slugify(tag.translated_name)
      log.printInfo("Adding new tag <%v> to <%v>", tag.name, alias)
      db.add_tag(tag.name, alias)
    end
  end

  if not db.add_image(image) then
    log.printError("failed to insert image!")
  end
end
