package config

import (
	"os"

	"github.com/jinzhu/configor"
	"gopkg.in/yaml.v2"
)

// Config parses the config file
type Config struct {
	Application Application `yaml:"application"`

	Runtime struct {
		HistoryFile  string `yaml:"history_file" default:"./.hist"`
		HistoryLimit int    `yaml:"history_limit" default:"500"`
	} `yaml:"runtime"`

	Database string `yaml:"database" default:"./imgbot.storm"`
	Assets   string `yaml:"assets" default:"./assets"`

	file string
}

// Load the configuration
func (c *Config) Load(file string) error {
	if file == "" {
		file = ApplicationName + ".yaml"
	}

	err := configor.New(&configor.Config{
		Debug:                false,
		Verbose:              false,
		Silent:               true,
		ErrorOnUnmatchedKeys: false,
		ENVPrefix:            ApplicationName,
	}).Load(
		c,
		file,
	)

	if err != nil {
		return err
	}

	c.file = file
	return nil
}

// Save the current config to disk
func (c *Config) Save() error {
	f, err := os.OpenFile(c.file, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, os.ModePerm)
	if err != nil {
		return err
	}

	return yaml.NewEncoder(f).Encode(c)
}
