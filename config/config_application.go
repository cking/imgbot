package config

// ApplicationScopes defines used oauth scopes
const ApplicationScopes = "read write follow"

// ApplicationWebsite for the application profile
const ApplicationWebsite = "https://gitea.z0ne.moe/cking/imgbot"

// ApplicationOOBRedirect defines the out of bounds redirect
const ApplicationOOBRedirect = "urn:ietf:wg:oauth:2.0:oob"

// Application config
type Application struct {
	Server       string `yaml:"server" default:"https://fedi.z0ne.moe"`
	ClientID     string `yaml:"client_id"`
	ClientSecret string `yaml:"client_secret"`
	AccessToken  string `yaml:"access_token"`
	AuthURI      string `yaml:"auth_uri"`
}

// IsRegistered checks if the application is registered
func (a *Application) IsRegistered() bool {
	return a.ClientID != "" && a.ClientSecret != ""
}
