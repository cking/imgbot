package model

// Tag model
type Tag struct {
	Original string `storm:"id"`
	Alias    string
}
