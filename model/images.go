package model

// Image model
type Image struct {
	File   string `storm:"unique"`
	Source string `storm:"id"`
	IsNSFW bool
	Tags   []string
	Author struct {
		Name    string
		Contact string
	}
}
