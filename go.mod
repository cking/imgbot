module gitea.z0ne.moe/cking/imgbot

go 1.12

require (
	github.com/AlecAivazis/survey/v2 v2.0.4
	github.com/DataDog/zstd v1.4.1 // indirect
	github.com/Sereal/Sereal v0.0.0-20190618215532-0b8ac451a863 // indirect
	github.com/asdine/storm v2.1.2+incompatible
	github.com/cjoudrey/gluahttp v0.0.0-20190104103309-101c19a37344
	github.com/cjoudrey/gluaurl v0.0.0-20161028222611-31cbb9bef199
	github.com/desertbit/grumble v1.0.3
	github.com/fatih/color v1.7.0
	github.com/golang/snappy v0.0.1 // indirect
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/gosimple/slug v1.7.0
	github.com/infogulch/uniq v0.0.0-20140513211423-b7eecd83d44d
	github.com/jinzhu/configor v1.1.1
	github.com/kohkimakimoto/gluafs v0.0.0-20160815050327-01391ed2d7ab
	github.com/kr/pretty v0.1.0 // indirect
	github.com/magefile/mage v1.8.0
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/mattn/go-mastodon v0.0.5-0.20190822052132-34e64bb423a3
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/olekukonko/tablewriter v0.0.1
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be // indirect
	github.com/robfig/cron v1.2.0
	github.com/rs/xid v1.2.1
	github.com/skratchdot/open-golang v0.0.0-20190402232053-79abb63cd66e
	github.com/vadv/gopher-lua-libs v0.0.5
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	github.com/yookoala/realpath v1.0.0 // indirect
	github.com/yuin/gopher-lua v0.0.0-20190514113301-1cd887cd7036
	gitlab.com/bakyun/magician v0.0.0-20190210101925-cf313cf20e1e
	go.etcd.io/bbolt v1.3.3 // indirect
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/sys v0.0.0-20190826190057-c7b8b68b1456 // indirect
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2
)

replace github.com/desertbit/grumble => github.com/cking/grumble v1.0.4-0.20190828210009-2ea5ec3e1753

replace github.com/desertbit/readline => github.com/cking/readline v0.0.0-20190827231441-51331e64b4c0

replace github.com/fatih/color => github.com/akyoto/color v1.8.6
