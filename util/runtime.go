package util

import (
	"time"
)

var startupTime = time.Now()

// RuntimeDuration returns the duration of the application runtime
func RuntimeDuration() time.Duration {
	return time.Now().Sub(startupTime)
}
