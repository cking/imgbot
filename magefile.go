//+build mage

package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"runtime"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/target"

	"gitlab.com/bakyun/magician"
)

var (
	isWin     = runtime.GOOS == "windows"
	tags      = ""
	generated = false
	Default   = Dev
)

func build(app string) error {
	exe := "out/" + app
	if isWin {
		exe += ".exe"
	}

	if !generated {
		magician.Exec("go", "generate", "./...")
		generated = true
	}

	files, err := ioutil.ReadDir(".")
	var paths []string
	for _, f := range files {
		if f.IsDir() && (f.Name() == "out" || f.Name()[0] == byte('.')) {
			continue
		}

		if !f.IsDir() && filepath.Ext(f.Name()) != ".go" {
			continue
		}

		paths = append(paths, f.Name())
	}

	build, err := target.Dir(exe, paths...)
	if err != nil {
		return err
	}

	if !build {
		magician.Echo("skipping, already up to date!")
		return nil
	}

	pkg := magician.GetGoPackageName("./config")
	root, _ := filepath.Abs(".")
	args := []string{
		"build", "-tags", tags, "-race",
		"-o", exe,
		"-ldflags", fmt.Sprintf("-X %v.BuildDir=%v %v", pkg, root, magician.GitVersionLDFlags(pkg)),
	}
	if mg.Verbose() {
		args = append(args, "-v")
	}

	args = append(args, "./cmd/"+app)

	return magician.Exec("go", args...)

}

func Build() error {
	magician.Headline("Build")
	return build("imgbot")
}

func Dev() {
	magician.Headline("Dev Mode")
	magician.Echo("enabling dev mode...")
	tags += " debug"
	mg.SerialDeps(Build)
}
