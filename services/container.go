package services

import (
	"context"
	"runtime"

	"gitea.z0ne.moe/cking/imgbot/config"
	"github.com/asdine/storm"
	"github.com/mattn/go-mastodon"
)

// Container for all dependencies
type Container struct {
	Configuration *config.Config

	Mastodon *mastodon.Client
	Storm    *storm.DB
}

// Build the container
func Build(cfg *config.Config) *Container {
	c := new(Container)
	c.Configuration = cfg

	c.buildMastodon()
	c.buildStorm()

	return c
}

func (c *Container) buildMastodon() {
	if c.Mastodon != nil {
		return
	}

	cfg := c.Configuration

	if !cfg.Application.IsRegistered() {
		app, err := mastodon.RegisterApp(context.Background(), &mastodon.AppConfig{
			Server:       cfg.Application.Server,
			ClientName:   config.ApplicationName,
			RedirectURIs: config.ApplicationOOBRedirect,
			Scopes:       config.ApplicationScopes,
			Website:      config.ApplicationWebsite,
		})

		if err != nil {
			panic(err)
		}

		cfg.Application.ClientID = app.ClientID
		cfg.Application.ClientSecret = app.ClientSecret
		cfg.Application.AuthURI = app.AuthURI

		if err := cfg.Save(); err != nil {
			panic(err)
		}
	}

	c.Mastodon = mastodon.NewClient(&mastodon.Config{
		Server:       cfg.Application.Server,
		ClientID:     cfg.Application.ClientID,
		ClientSecret: cfg.Application.ClientSecret,
		AccessToken:  cfg.Application.AccessToken,
	})
}

func (c *Container) buildStorm() {
	if c.Storm != nil {
		return
	}

	db, err := storm.Open(c.Configuration.Database)
	if err != nil {
		panic(err)
	}

	runtime.SetFinalizer(db, func(db *storm.DB) {
		db.Close()
	})

	c.Storm = db
}
