package lmodules

import (
	"path/filepath"
	"strings"

	"github.com/rs/xid"

	"github.com/gosimple/slug"

	"gitea.z0ne.moe/cking/imgbot/model"

	"gitea.z0ne.moe/cking/imgbot/config"
	"github.com/asdine/storm"
	lua "github.com/yuin/gopher-lua"
)

// DBLoader for lua
func DBLoader(cfg *config.Config, db *storm.DB) lua.LGFunction {
	funcs := map[string]lua.LGFunction{
		"image_file": func(L *lua.LState) int {
			L.Push(lua.LString(filepath.Join(cfg.Assets, xid.New().String()+L.ToString(1))))
			return 1
		},
		"slugify": func(L *lua.LState) int {
			L.Push(lua.LString(
				strings.ReplaceAll( // replace - with _, better fedi compat
					slug.Make(L.ToString(1)),
					"-",
					"_",
				),
			))
			return 1
		},
		"has_tag": func(L *lua.LState) int {
			q := L.ToString(1)
			var ms []model.Tag
			if err := db.Find("Original", q, &ms); err != nil {
				L.Push(lua.LNil)
				L.Push(lua.LString(err.Error()))
			} else {
				L.Push(lua.LNumber(len(ms)))
				L.Push(lua.LNil)
			}
			return 2
		},
		"add_tag": func(L *lua.LState) int {
			m := model.Tag{
				Original: L.ToString(1),
				Alias:    L.ToString(2),
			}

			if m.Original == "" {
				L.Push(lua.LFalse)
			}

			if err := db.Save(&m); err != nil {
				L.Push(lua.LFalse)
			} else {
				L.Push(lua.LTrue)
			}
			return 1
		},
		"add_image": func(L *lua.LState) int {
			t := L.ToTable(1)

			if t == nil {
				L.Push(lua.LFalse)
			} else {
				i := new(model.Image)
				i.File = t.RawGetString("File").String()
				i.Source = t.RawGetString("Source").String()
				a := t.RawGetString("Author").(*lua.LTable)
				i.Author.Name = a.RawGetString("Name").String()
				i.Author.Contact = a.RawGetString("Contact").String()

				tags := t.RawGetString("Tags").(*lua.LTable)
				for idx := 1; idx <= tags.Len(); idx++ {
					i.Tags = append(i.Tags, tags.RawGetInt(idx).String())
				}

				i.IsNSFW = bool(t.RawGetString("IsNSFW").(lua.LBool))

				if i.Source == "" {
					L.Push(lua.LFalse)
				} else {
					err := db.Save(i)
					L.Push(lua.LBool(err == nil))
				}
			}

			return 1
		},
	}

	vars := map[string]lua.LValue{}

	return func(L *lua.LState) int {
		t := L.NewTable()
		L.SetFuncs(t, funcs)

		for k, v := range vars {
			L.SetField(t, k, v)
		}

		L.Push(t)
		return 1
	}
}
