package lmodules

import (
	lua "github.com/yuin/gopher-lua"
)

func loggerWrapper(p func(string, ...interface{})) lua.LGFunction {
	return func(L *lua.LState) int {
		args := L.GetTop()
		if args < 1 {
			L.RaiseError("not enough arguments.")
			return 0
		}

		a := make([]interface{}, args-1)
		for i := 2; i <= args; i++ {
			a[i-2] = L.Get(i)
		}

		p(
			L.ToString(1),
			a...,
		)

		return 0
	}
}

// LoggerLoader for lua
func LoggerLoader(p LoggerProvider) lua.LGFunction {
	funcs := map[string]lua.LGFunction{
		"print":        loggerWrapper(p.Print),
		"printDebug":   loggerWrapper(p.PrintDebug),
		"printInfo":    loggerWrapper(p.PrintInfo),
		"printSuccess": loggerWrapper(p.PrintSuccess),
		"printWarning": loggerWrapper(p.PrintWarning),
		"printError":   loggerWrapper(p.PrintError),
	}

	return func(L *lua.LState) int {
		t := L.NewTable()
		L.SetFuncs(t, funcs)
		L.Push(t)
		return 1
	}
}
