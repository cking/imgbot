package lmodules

import (
	"github.com/AlecAivazis/survey/v2"
	lua "github.com/yuin/gopher-lua"
)

var surveyFuncs = map[string]lua.LGFunction{
	"input": func(L *lua.LState) int {
		p := survey.Input{
			Message: L.ToString(1),
			Default: L.ToString(2),
			Help:    L.ToString(3),
		}
		var res string
		if err := survey.AskOne(&p, &res); err != nil {
			L.Push(lua.LString(""))
			L.Push(lua.LFalse)
			return 2
		}

		L.Push(lua.LString(res))
		L.Push(lua.LTrue)
		return 2
	},
	"multiline": func(L *lua.LState) int {
		p := survey.Multiline{
			Message: L.ToString(1),
			Default: L.ToString(2),
			Help:    L.ToString(3),
		}
		var res string
		if err := survey.AskOne(&p, &res); err != nil {
			L.Push(lua.LString(""))
			L.Push(lua.LFalse)
			return 2
		}

		L.Push(lua.LString(res))
		L.Push(lua.LTrue)
		return 2
	},
	"password": func(L *lua.LState) int {
		p := survey.Password{
			Message: L.ToString(1),
			Help:    L.ToString(2),
		}
		var res string
		if err := survey.AskOne(&p, &res); err != nil {
			L.Push(lua.LString(""))
			L.Push(lua.LFalse)
			return 2
		}

		L.Push(lua.LString(res))
		L.Push(lua.LTrue)
		return 2
	},
	"confirm": func(L *lua.LState) int {
		p := survey.Confirm{
			Message: L.ToString(1),
			Default: L.ToBool(2),
			Help:    L.ToString(3),
		}
		var res bool
		if err := survey.AskOne(&p, &res); err != nil {
			L.Push(lua.LFalse)
			L.Push(lua.LFalse)
			return 2
		}

		L.Push(lua.LBool(res))
		L.Push(lua.LTrue)
		return 2
	},
	"select": func(L *lua.LState) int {
		t := L.ToTable(2)
		options := make([]string, t.Len())
		for i := 0; i < len(options); i++ {
			options[i] = t.RawGetInt(i + 1).String()
		}

		p := survey.Select{
			Message:  L.ToString(1),
			Options:  options,
			Help:     L.ToString(3),
			PageSize: L.ToInt(4),
		}

		var res survey.OptionAnswer
		if err := survey.AskOne(&p, &res); err != nil {
			L.Push(lua.LFalse)
			L.Push(lua.LFalse)
			return 2
		}

		L.Push(lua.LString(res.Value))
		L.Push(lua.LTrue)
		return 2
	},
	"multiselect": func(L *lua.LState) int {
		t := L.ToTable(2)
		options := make([]string, t.Len())
		for i := 0; i < len(options); i++ {
			options[i] = t.RawGetInt(i + 1).String()
		}

		p := survey.MultiSelect{
			Message:  L.ToString(1),
			Options:  options,
			Help:     L.ToString(3),
			PageSize: L.ToInt(4),
		}

		var res []survey.OptionAnswer
		if err := survey.AskOne(&p, &res); err != nil {
			L.Push(lua.LFalse)
			L.Push(lua.LFalse)
			return 2
		}

		t = L.NewTable()
		for i, a := range res {
			t.RawSetInt(i+1, lua.LString(a.Value))
		}
		L.Push(t)
		L.Push(lua.LTrue)
		return 2
	},
}

// SurveyLoader for lua
func SurveyLoader(L *lua.LState) int {
	t := L.NewTable()
	L.SetFuncs(t, surveyFuncs)
	L.Push(t)
	return 1
}
