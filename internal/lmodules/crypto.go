package lmodules

import (
	"crypto/md5"
	"encoding/hex"

	lua "github.com/yuin/gopher-lua"
)

var cryptoFuncs = map[string]lua.LGFunction{
	"md5": func(L *lua.LState) int {
		hasher := md5.New()
		hasher.Write([]byte(L.ToString(1)))

		L.Push(lua.LString(hex.EncodeToString(hasher.Sum(nil))))
		return 1
	},
}

// CryptoLoader for lua
func CryptoLoader(L *lua.LState) int {
	t := L.NewTable()
	L.SetFuncs(t, cryptoFuncs)
	L.Push(t)
	return 1
}
