package lmodules

// LoggerProvider interface
type LoggerProvider interface {
	Print(format string, args ...interface{})
	PrintDebug(format string, args ...interface{})
	PrintInfo(format string, args ...interface{})
	PrintSuccess(format string, args ...interface{})
	PrintWarning(format string, args ...interface{})
	PrintError(format string, args ...interface{})
}
