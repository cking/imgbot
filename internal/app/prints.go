package app

import (
	"fmt"
	"strings"
	"time"

	"github.com/fatih/color"
)

// Print message
func (a *App) print(prefix string, format string, args ...interface{}) {
	now := time.Now().Format(time.RFC3339Nano)
	fmt.Fprintf(
		color.Output,
		"\r[%v] <%v> ",
		color.HiBlackString(now)+strings.Repeat(" ", len(time.RFC3339Nano)-len(now)),
		prefix,
	)
	fmt.Fprintf(color.Output, format, args...)
	fmt.Println()

	a.grumble.Refresh()
}

// Print message
func (a *App) Print(format string, args ...interface{}) {
	a.print("       ", format, args...)
}

// PrintDebug message
func (a *App) PrintDebug(format string, args ...interface{}) {
	a.print(color.HiMagentaString("DEBUG  "), format, args...)
}

// PrintInfo message
func (a *App) PrintInfo(format string, args ...interface{}) {
	a.print(color.CyanString("INFO   "), format, args...)
}

// PrintSuccess message
func (a *App) PrintSuccess(format string, args ...interface{}) {
	a.print(color.GreenString("SUCCESS"), format, args...)
}

// PrintWarning message
func (a *App) PrintWarning(format string, args ...interface{}) {
	a.print(color.YellowString("WARNING"), format, args...)
}

// PrintError message
func (a *App) PrintError(format string, args ...interface{}) {
	a.print(color.RedString("ERROR  "), format, args...)
}

// PrintErr messages
func (a *App) PrintErr(err error) {
	a.PrintError("%v", err)
}
