package app

import "errors"

// The application errors
var (
	ErrNoImages = errors.New("no images in database")
)
