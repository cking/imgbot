package app

import (
	"context"
	"os"
	"sync"
	"time"

	"gitea.z0ne.moe/cking/imgbot/config"
	"gitea.z0ne.moe/cking/imgbot/services"
	"github.com/AlecAivazis/survey/v2"
	"github.com/desertbit/grumble"
	"github.com/robfig/cron"
	"github.com/skratchdot/open-golang/open"
)

// App struct
type App struct {
	grumble    *grumble.App
	container  *services.Container
	cron       *cron.Cron
	imageIndex int
	cronMutex  sync.Mutex
}

// New grumble application
func New() *App {
	a := new(App)
	a.grumble = grumble.New(&grumble.Config{
		Name:        config.ApplicationName,
		Description: "image bot for mastodon using a definite list of images as the source",
		Flags: func(f *grumble.Flags) {
			f.String("c", "config", "", "Specify a custom config file to load")
			f.Bool("b", "batch", false, "Disable interactive mode and just run the cronjob")
		},

		NoColor: true,
		Prompt:  config.ApplicationName + "$ ",
	})

	a.grumble.OnInit(a.onInit)
	a.grumble.OnShell(a.onShell)
	a.grumble.OnClose(a.onClose)

	a.registerMastodonCommands()
	a.registerStormCommands()
	a.registerLuaCommands()

	return a
}

func (a *App) onInit(app *grumble.App, flags grumble.FlagMap) error {
	cfg := new(config.Config)
	err := cfg.Load(flags.String("config"))
	if err != nil {
		return err
	}

	a.container = services.Build(cfg)

	ac := app.Config()
	ac.HistoryFile = cfg.Runtime.HistoryFile
	ac.HistoryLimit = cfg.Runtime.HistoryLimit

	a.Print("%v v%v [%v] launching", config.ApplicationName, config.Version, config.GitCommit)

	a.PrintDebug("Logging in to Mastodon...")
	if a.container.Configuration.Application.AccessToken == "" {
		a.PrintInfo(
			"No access token set, please visit the following webpage to create a new one:\n %v",
			a.container.Configuration.Application.AuthURI,
		)
		var openPage bool
		err := survey.AskOne(&survey.Confirm{
			Message: "Open the default browser now?",
			Default: false,
			Help:    "This will open the browser with the specified on the computer this bot is running on.",
		}, &openPage)
		if err != nil {
			a.PrintError("failed to query for user input: %v", err)
			return err
		}

		if openPage {
			if err := open.Start(a.container.Configuration.Application.AuthURI); err != nil {
				a.PrintWarning("failed to launch the browser, please copy and past manually")
			}
		}

		var token string
		if err := survey.AskOne(&survey.Input{Message: "Enter the token"}, &token); err != nil {
			a.PrintError("failed to query for user input: %v", err)
			return err
		}

		if err := a.container.Mastodon.AuthenticateToken(context.Background(), token, config.ApplicationOOBRedirect); err != nil {
			a.PrintError("failed to authenticate with the token: %v", err)
			return err
		}

		a.container.Configuration.Application.AccessToken = a.container.Mastodon.Config.AccessToken
		if err := a.container.Configuration.Save(); err != nil {
			a.PrintError("failed to store the access token: %v", err)
			return err
		}
	}
	acc, err := a.container.Mastodon.GetAccountCurrentUser(context.Background())
	if err != nil {
		a.PrintError("failed to fetch account details: %v", err)
		a.Print("if this continues, delete the access_token from your configuration file")
		return err
	}
	a.PrintSuccess("Successfully logged in as %v!", acc.Acct)

	return nil
}

func (a *App) onShell(app *grumble.App) error {
	a.cron = cron.New()
	a.cron.AddFunc("0 */30 * * * *", func() {
		a.cronMutex.Lock()
		defer a.cronMutex.Unlock()
		a.Post()
	})

	go func() {
		// dont start cron to early pretty please
		<-time.After(time.Second)
		a.cron.Start()
	}()
	return nil
}

func (a *App) onClose() error {
	if a.cron != nil {
		a.cron.Stop()
	}

	return nil
}

// Run the application
func (a *App) Run() {
	err := a.grumble.Run()
	if err != nil && err != ErrNoImages {
		a.PrintErr(err)
		os.Exit(1)
	}
}
