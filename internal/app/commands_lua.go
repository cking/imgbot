package app

import (
	"net/http"
	"path/filepath"

	"github.com/kohkimakimoto/gluafs"

	"gitea.z0ne.moe/cking/imgbot/internal/lmodules"
	"github.com/cjoudrey/gluahttp"
	"github.com/cjoudrey/gluaurl"
	"github.com/desertbit/grumble"
	lfp "github.com/vadv/gopher-lua-libs/filepath"
	linspect "github.com/vadv/gopher-lua-libs/inspect"
	ljson "github.com/vadv/gopher-lua-libs/json"
	lregexp "github.com/vadv/gopher-lua-libs/regexp"
	lua "github.com/yuin/gopher-lua"
)

func (a *App) registerLuaCommands() {
	a.grumble.AddCommand(&grumble.Command{
		Name:      "lua",
		Aliases:   []string{"run", "r"},
		Help:      "Run a defined lua script",
		HelpGroup: "Mastodon",
		AllowArgs: true,
		Run: func(c *grumble.Context) error {
			if len(c.Args) < 1 {
				a.PrintError("not enough arguments, provide the lua filename and the arguments")
				return nil
			}

			a.cronMutex.Lock()
			defer a.cronMutex.Unlock()

			L := lua.NewState()
			defer L.Close()
			L.PreloadModule("http", gluahttp.NewHttpModule(http.DefaultClient).Loader)
			L.PreloadModule("url", gluaurl.Loader)
			L.PreloadModule("filepath", lfp.Loader)
			L.PreloadModule("fs", gluafs.Loader)
			L.PreloadModule("inspect", linspect.Loader)
			L.PreloadModule("json", ljson.Loader)
			L.PreloadModule("regexp", lregexp.Loader)

			L.PreloadModule("logger", lmodules.LoggerLoader(a))
			L.PreloadModule("db", lmodules.DBLoader(a.container.Configuration, a.container.Storm))
			L.PreloadModule("survey", lmodules.SurveyLoader)
			L.PreloadModule("crypto", lmodules.CryptoLoader)

			file := c.Args[0]
			if filepath.Ext(file) == "" {
				file += ".lua"
			}

			/*
				L.SetGlobal("mastodon", luar.New(L, a.container.Mastodon))
			*/
			if err := L.DoFile(file); err != nil {
				a.PrintError("failed to parse script file: %v", err)
				return nil
			}

			args := L.NewTable()
			for _, a := range c.Args[1:] {
				args.Append(lua.LString(a))
			}

			if err := L.CallByParam(lua.P{
				Fn:      L.GetGlobal("main"), // name of Lua function
				NRet:    1,                   // number of returned values
				Protect: true,                // return err or panic
			}, args); err != nil {
				if aerr, ok := err.(*lua.ApiError); ok {
					if aerr.Type == lua.ApiErrorRun {
						a.PrintError("script exited with error: %v", aerr.Object)
						return nil
					}
				}
				a.PrintError("failed to run script: %v", err)
			}

			return nil
		},
	})
}
