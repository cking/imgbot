package app

import (
	"context"
	"fmt"
	"math/rand"
	"sort"
	"strings"

	"github.com/infogulch/uniq"

	"gitea.z0ne.moe/cking/imgbot/model"
	"github.com/asdine/storm"
	"github.com/mattn/go-mastodon"
	"golang.org/x/xerrors"
)

// Post an image
func (app *App) Post() error {
	db := app.container.Storm
	app.PrintDebug("posting new image...")

	c, err := db.Count(new(model.Image))
	if err != nil {
		app.PrintError("failed to count images")
		return xerrors.Errorf("failed to count available images: %w", err)
	}

	if c == 0 {
		app.PrintError("no images in database")
		return ErrNoImages
	}

	r := int(rand.Float64() * float64(c))
	app.imageIndex += r
	if app.imageIndex > c {
		app.imageIndex -= c
	}

	var img []model.Image
	err = db.All(&img, storm.Limit(1), storm.Skip(app.imageIndex))
	if err != nil {
		app.PrintError("failed to fetch image metadata: %v", err)
		return xerrors.Errorf("failed to fetch image metadata: %w", err)
	}

	at, err := app.container.Mastodon.UploadMedia(context.Background(), img[0].File)
	if err != nil {
		app.PrintError("failed to upload image: %v", err)
		return xerrors.Errorf("failed to upload image: %w", err)
	}

	tags := make([]string, 0, len(img[0].Tags))
	for _, t := range img[0].Tags {
		newTag := new(model.Tag)
		err := db.One("Original", t, newTag)
		if err != nil {
			tags = append(tags, "#"+t)
		} else if newTag.Alias != "" {
			tags = append(tags, "#"+newTag.Alias)
		}
	}
	uniqs := uniq.Strings(tags)
	tags = tags[:uniqs-1]
	sort.Strings(tags)

	_, err = app.container.Mastodon.PostStatus(context.Background(), &mastodon.Toot{
		Status: fmt.Sprintf(`
Sauce: %v
Author: %v (%v)
Tags: %v`, img[0].Source, img[0].Author.Name, img[0].Author.Contact, strings.Join(tags, " ")),
		MediaIDs:   []mastodon.ID{at.ID},
		Sensitive:  img[0].IsNSFW,
		Visibility: "unlisted",
	})

	if err != nil {
		app.PrintError("failed to post new status: %v", err)
		return xerrors.Errorf("failed to post new status: %w", err)
	}
	return nil
}
