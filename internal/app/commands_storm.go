package app

import (
	"os"
	"regexp"

	"github.com/asdine/storm/q"

	"gitea.z0ne.moe/cking/imgbot/model"
	"gitea.z0ne.moe/cking/imgbot/util"
	"github.com/asdine/storm"
	"github.com/desertbit/grumble"
	"github.com/fatih/color"
	"github.com/olekukonko/tablewriter"
)

func (a *App) registerStormCommands() {
	a.grumble.AddCommand(&grumble.Command{
		Name:      "dbinfo",
		Help:      "Database info",
		HelpGroup: "Database",
		AllowArgs: false,
		Run: func(c *grumble.Context) error {
			db := a.container.Storm

			tagCount, err := db.Count(new(model.Tag))
			if err != nil {
				a.PrintError("failed to retrieve tag count: %v", err)
				return nil
			}

			imageCount, err := db.Count(new(model.Image))
			if err != nil {
				a.PrintError("failed to retrieve tag count: %v", err)
				return nil
			}

			a.Print("Runtime:          %v", util.RuntimeDuration())
			a.Print("Tag replacements: %v", tagCount)
			a.Print("Images:           %v", imageCount)

			return nil
		},
	})

	a.grumble.AddCommand(&grumble.Command{
		Name:      "tags",
		Help:      "List tags",
		HelpGroup: "Database",
		AllowArgs: true,
		Flags: func(f *grumble.Flags) {
			f.Int("p", "page", 1, "Page to display")
			f.Int("c", "count", 25, "Number of entries per page")
		},
		Run: func(c *grumble.Context) error {
			db := a.container.Storm
			s := ""
			if len(c.Args) > 0 {
				s = c.Args[0]
			}

			var tags []model.Tag
			var err error
			if s == "" {
				err = db.All(&tags,
					storm.Limit(c.Flags.Int("count")),
					storm.Skip(c.Flags.Int("count")*(c.Flags.Int("page")-1)))
			} else {
				err = db.Select(q.Re("Original", regexp.QuoteMeta(s))).
					Limit(c.Flags.Int("count")).
					Skip(c.Flags.Int("count") * (c.Flags.Int("page") - 1)).
					Find(&tags)
			}
			if err != nil && err != storm.ErrNotFound {
				a.PrintError("Failed to query database: %v", err)
				return nil
			}

			if len(tags) == 0 {
				a.PrintWarning("No results on page %v", c.Flags.Int("page"))
				return nil
			}

			table := tablewriter.NewWriter(color.Output)
			table.SetHeader([]string{"Original", "Replacement"})

			for _, tag := range tags {
				alias := tag.Alias
				if alias == "" {
					alias = color.RedString("delete")
				}
				table.Append([]string{
					tag.Original,
					alias,
				})
			}

			table.Render()

			return nil
		},
	})

	a.grumble.AddCommand(&grumble.Command{
		Name:      "puttag",
		Help:      "Add a new tag replacer or modify one",
		HelpGroup: "Database",
		AllowArgs: true,
		Run: func(c *grumble.Context) error {
			if len(c.Args) < 1 || len(c.Args) > 2 {
				a.PrintError("Please specify 1 or 2 arguments, the original tag and the replacement. Leave the replacement empty to remove the tag")
				return nil
			}

			tag := new(model.Tag)
			tag.Original = c.Args[0]
			if len(c.Args) == 2 {
				tag.Alias = c.Args[1]
			}

			db := a.container.Storm
			if err := db.Save(tag); err != nil {
				a.PrintError("failed to save new tag: %v", err)
				return nil
			}

			a.PrintSuccess("Tag replacer added or updated")

			return nil
		},
	})

	a.grumble.AddCommand(&grumble.Command{
		Name:      "deltag",
		Help:      "Remove a tag",
		HelpGroup: "Database",
		AllowArgs: true,
		Run: func(c *grumble.Context) error {
			if len(c.Args) != 1 {
				a.PrintError("Please specify the tag to remove")
				return nil
			}

			tag := new(model.Tag)
			db := a.container.Storm
			if err := db.One("Original", c.Args[0], tag); err != nil {
				if err == storm.ErrNotFound {
					a.PrintSuccess("tag not found")
					return nil
				}

				a.PrintError("failed to search for tag: %v", err)
				return nil
			}
			if err := db.DeleteStruct(tag); err != nil {
				a.PrintError("failed to delete tag: %v", err)
				return nil
			}

			a.PrintSuccess("Tag replacer deleted")
			return nil
		},
	})

	a.grumble.AddCommand(&grumble.Command{
		Name:      "images",
		Help:      "List images",
		HelpGroup: "Database",
		AllowArgs: true,
		Flags: func(f *grumble.Flags) {
			f.Int("p", "page", 1, "Page to display")
			f.Int("c", "count", 25, "Number of entries per page")
		},
		Run: func(c *grumble.Context) error {
			db := a.container.Storm
			s := ""
			if len(c.Args) > 0 {
				s = c.Args[0]
			}

			var images []model.Image
			var err error
			if s == "" {
				err = db.All(&images,
					storm.Limit(c.Flags.Int("count")),
					storm.Skip(c.Flags.Int("count")*(c.Flags.Int("page")-1)))
			} else {
				err = db.Select(q.Re("Source", regexp.QuoteMeta(s))).
					Limit(c.Flags.Int("count")).
					Skip(c.Flags.Int("count") * (c.Flags.Int("page") - 1)).
					Find(&images)
			}
			if err != nil && err != storm.ErrNotFound {
				a.PrintError("Failed to query database: %v", err)
				return nil
			}

			if len(images) == 0 {
				a.PrintWarning("No results on page %v", c.Flags.Int("page"))
				return nil
			}

			table := tablewriter.NewWriter(color.Output)
			table.SetHeader([]string{"Source", "File", "IsNSFW", "Tags"})
			table.SetAutoMergeCells(true)

			for _, i := range images {
				isNSFW := color.RedString("false")
				if i.IsNSFW {
					isNSFW = color.GreenString("true")
				}

				f := i.File
				if _, err := os.Stat(f); os.IsNotExist(err) {
					f = color.YellowString(f)
				}

				for _, t := range i.Tags {
					table.Append([]string{
						i.Source,
						f,
						isNSFW,
						t,
					})

					isNSFW = ""
				}
			}

			table.Render()

			return nil
		},
	})

	a.grumble.AddCommand(&grumble.Command{
		Name:      "nsfw",
		Help:      "Toggle nsfw flag on image",
		HelpGroup: "Database",
		AllowArgs: true,
		Run: func(c *grumble.Context) error {
			if len(c.Args) != 1 {
				a.PrintError("Please specify the source image url")
				return nil
			}

			image := new(model.Image)
			db := a.container.Storm
			if err := db.One("Source", c.Args[0], image); err != nil {
				if err == storm.ErrNotFound {
					a.PrintWarning("no image found for <%v>", c.Args[0])
				} else {
					a.PrintError("failed to query database: %v", err)
				}
				return nil
			}

			image.IsNSFW = !image.IsNSFW

			if err := db.Save(image); err != nil {
				a.PrintError("failed to update image: %v", err)
				return nil
			}

			a.PrintSuccess("image nsfw tag changed to <%v>", image.IsNSFW)

			return nil
		},
	})

	a.grumble.AddCommand(&grumble.Command{
		Name:      "delimage",
		Help:      "Remove a tag",
		HelpGroup: "Database",
		AllowArgs: true,
		Flags: func(f *grumble.Flags) {
			f.Bool("n", "no-delete", false, "don't delete image on disk")
		},
		Run: func(c *grumble.Context) error {
			if len(c.Args) != 1 {
				a.PrintError("Please specify the source image url")
				return nil
			}

			image := new(model.Image)
			db := a.container.Storm
			if err := db.One("Source", c.Args[0], image); err != nil {
				if err == storm.ErrNotFound {
					a.PrintWarning("no image found for <%v>", c.Args[0])
				} else {
					a.PrintError("failed to query database: %v", err)
				}
				return nil
			}

			if err := db.DeleteStruct(image); err != nil {
				a.PrintError("failed to delete image: %v", err)
				return nil
			}

			if !c.Flags.Bool("no-delete") {
				if err := os.Remove(image.File); err != nil {
					a.PrintError("failed to delete <%v>: %v", image.File, err)
				} else {
					a.PrintDebug("deleted image file on disk")
				}
			}

			a.PrintSuccess("image deleted")
			return nil
		},
	})

	a.grumble.AddCommand(&grumble.Command{
		Name:      "addimagetag",
		Aliases:   []string{"iat", "ait"},
		Help:      "Add a tag to an image",
		HelpGroup: "Database",
		AllowArgs: true,
		Run: func(c *grumble.Context) error {
			if len(c.Args) != 2 {
				a.PrintError("Please specify the source image url and the tag to add")
				return nil
			}

			image := new(model.Image)
			db := a.container.Storm
			if err := db.One("Source", c.Args[0], image); err != nil {
				if err == storm.ErrNotFound {
					a.PrintWarning("no image found for <%v>", c.Args[0])
				} else {
					a.PrintError("failed to query database: %v", err)
				}
				return nil
			}

			image.Tags = append(image.Tags, c.Args[1])

			if err := db.Save(image); err != nil {
				a.PrintError("failed to update image: %v", err)
				return nil
			}

			a.PrintSuccess("image tag <%v> added", c.Args[1])

			return nil
		},
	})

	a.grumble.AddCommand(&grumble.Command{
		Name:      "delimagetag",
		Aliases:   []string{"idt", "dit"},
		Help:      "Add a tag to an image",
		HelpGroup: "Database",
		AllowArgs: true,
		Run: func(c *grumble.Context) error {
			if len(c.Args) != 2 {
				a.PrintError("Please specify the source image url and the tag to add")
				return nil
			}

			image := new(model.Image)
			db := a.container.Storm
			if err := db.One("Source", c.Args[0], image); err != nil {
				if err == storm.ErrNotFound {
					a.PrintWarning("no image found for <%v>", c.Args[0])
				} else {
					a.PrintError("failed to query database: %v", err)
				}
				return nil
			}

			newTags := make([]string, 0, len(image.Tags))
			for _, tag := range image.Tags {
				if tag != c.Args[1] {
					newTags = append(newTags, tag)
				}
			}
			image.Tags = newTags

			if err := db.Save(image); err != nil {
				a.PrintError("failed to update image: %v", err)
				return nil
			}

			a.PrintSuccess("image tag <%v> removed", c.Args[1])

			return nil
		},
	})
}
