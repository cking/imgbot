package app

import (
	"context"

	"github.com/desertbit/grumble"
)

func (a *App) registerMastodonCommands() {
	a.grumble.AddCommand(&grumble.Command{
		Name:    "whoami",
		Aliases: []string{},
		Help:    "Retrieve your Mastodon account",
		// LongHelp: "",
		HelpGroup: "Mastodon",
		// Usage: "",
		// Flags: func (f*grumble.Flags){},
		AllowArgs: false,
		Run: func(c *grumble.Context) error {
			acc, err := a.container.Mastodon.GetAccountCurrentUser(context.Background())
			if err != nil {
				a.PrintError("failed to retrieve account data: %v", err)
				return nil
			}

			a.Print("ID:             %v", acc.ID)
			a.Print("Username:       %v", acc.Username)
			a.Print("Acct:           %v", acc.Acct)
			a.Print("DisplayName:    %v", acc.DisplayName)
			a.Print("FollowersCount: %v", acc.FollowersCount)
			a.Print("FollowingCount: %v", acc.FollowingCount)

			return nil
		},
		// Completer: func(prefix string, args []string) []string {}
	})

	a.grumble.AddCommand(&grumble.Command{
		Name:      "post",
		Aliases:   []string{},
		Help:      "Post a new image",
		HelpGroup: "Mastodon",
		AllowArgs: false,
		Run: func(c *grumble.Context) error {
			a.Post()
			return nil
		},
	})
}
